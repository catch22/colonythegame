import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux'
import ReactDOM from 'react-dom';
import {Layer, Rect, Line, Circle, Stage, Group} from 'react-konva';

// App modules
import * as Common from './common'
import * as Ship from './ship'
import * as Utils from './utils'
import {mapSelected} from './actions'
import * as Logic from './logic'

function mycolor() {
  return 'orange'
}
class _Map extends React.PureComponent {
    constructor() {
      super();
      this.state = {showSectors: true,
                    hoveri: -1,
                    hoverj: -1,
                    currentSector: -1
                  }
    }
    componentDidMount() {

    }

    filterSector(colony) {
      return (colony.coords[0] == this.state.currentSector)
    }
    onClick(id) {
      var cs = id
      if (this.state.showSectors) {
        // Sector selected
      } else {
        // Colony selected
        this.props.onMapSelected(id)
        cs = -1
      }
      this.setState({
        showSectors: !this.state.showSectors,
        currentSector: cs,
        hoveri: -1,
        hoverj: -1})
    }

    onMouseEnter(i,j) {
      this.setState({hoveri: i,
                      hoverj: j})
    }

    onMouseLeave(i,j) {
      this.setState({hoveri: -1,
                      hoverj: -1})
    }

    render() {
      let {colonies, ships, state} = this.props
      return (
        <div>
        {this.state.showSectors &&
          <div>
          <div><b> Select a sector</b></div>
          {(-1!=this.state.hoveri && -1!=this.state.hoverj) ?
            <span>Sector: {Logic.sector_id(this.state.hoveri,this.state.hoverj)}
            </span> :
            <span>&nbsp;</span>
          }
          <Stage width={Logic.STAGE_WIDTH} height={Logic.STAGE_HEIGHT}>
            <Layer>
             {Utils.range(0,Logic.SECTOR_WIDTH).map((i) => (
               Utils.range(0,Logic.SECTOR_HEIGHT).map((j) => (
                 <Rect key={Logic.sector_id(i,j)} x={Logic.sector_x(i,j)} y={Logic.sector_y(i,j)} width={Logic.SECTOR_SQUARE} height={Logic.SECTOR_SQUARE}
                  stroke={(i==this.state.hoveri && j==this.state.hoverj)?'#f33':'#aaa'}
                  onClick={(e) => (this.onClick(Logic.sector_id(i,j),'sector'))}
                  onMouseEnter={(e) => this.onMouseEnter(i,j,e)}
                  onMouseLeave={(e) => this.onMouseLeave(i,j,e)}
                  />
               ))

             ))}

             {colonies.map(function(colony) {
                var coords = Logic.sector_coords(colony.coords[0])
                var i = coords[0]
                var j = coords[1]

                // Mark sectors with colonies
                return (
                <Rect key={Logic.sector_id(i,j)} x={Logic.sector_x(i,j)+4} y={Logic.sector_y(i,j)+4} width={Logic.SECTOR_SQUARE-8} height={Logic.SECTOR_SQUARE-8}
                 fill='blue'

                 />)
             }

             )}

             {ships.filter((s) => (s.type == 'lander' && s.progress >= 0)).map(function(ship) {
               //var sec_coords = Logic.sector_coords(ship.target[0])
               //var scale = ship.progress/ship.distance
               //ship.position = sec_coords.map((n) => (n*scale))
               if (!ship.target_sector) {
                 ship.target_sector = ship.position_sector
               }
               // Draw landers
               return (
                 <Group key={ship.id}>
                 <Circle key={ship.id}
                         x={Logic.sector_x(ship.position_sector[0], ship.position_sector[1])+Logic.SECTOR_SQUARE/2}
                         y={Logic.sector_y(ship.position_sector[0], ship.position_sector[1])+Logic.SECTOR_SQUARE/2}
                  width={10} height={10} fill={Utils.lander_color(ship)}/>
                 <Line x={0} y={0}
                    points={[Logic.sector_x(ship.position_sector[0], ship.position_sector[1])+Logic.SECTOR_SQUARE/2,
                             Logic.sector_y(ship.position_sector[0], ship.position_sector[1])+Logic.SECTOR_SQUARE/2,
                             Logic.sector_x(ship.target_sector[0], ship.target_sector[1])+Logic.SECTOR_SQUARE/2,
                             Logic.sector_y(ship.target_sector[0], ship.target_sector[1])+Logic.SECTOR_SQUARE/2]}
                    stroke={Utils.lander_color(ship)}
                    strokeWidth={1}
                    dash={[10, 5, 1, 5]}
                     />
                  </Group>
                )
              })
             }
            </Layer>
            </Stage>
            </div>
        }

        <div>
        {!this.state.showSectors &&
          <div>
            <div><b> Sector: {this.state.currentSector} </b></div>
          {(-1!=this.state.hoveri && -1!=this.state.hoverj) ?
            <span>Colony: {Logic.colony_id(this.state.currentSector, this.state.hoveri,this.state.hoverj)}
            </span> :
            <span>&nbsp;</span>
          }
            <Stage width={700} height={700}>
            <Layer>
             {Utils.range(0,Logic.COLONY_WIDTH).map((i) => (
               Utils.range(0,Logic.COLONY_HEIGHT).map((j) => (
                 <Circle key={Logic.sector_id(i,j)} x={Logic.colony_x(i,j)} y={Logic.colony_y(i,j)} radius={5}
                 stroke={(i==this.state.hoveri && j==this.state.hoverj)?'#f33':'#aaa'}
                  onClick={(e) => (this.onClick(Logic.colony_id(this.state.currentSector,i,j)))}
                  onMouseEnter={(e) => this.onMouseEnter(i,j,e)}
                  onMouseLeave={(e) => this.onMouseLeave(i,j,e)}
                  />
               ))

             ))}

             {colonies.filter(this.filterSector.bind(this)).map(function(colony) {
                var coords = Logic.colony_subcoords(colony.coords[1])
                var i = coords[0]
                var j = coords[1]
                return (
                <Circle key={Logic.sector_id(i,j)} x={Logic.colony_x(i,j)} y={Logic.colony_y(i,j)} radius={3}
                fill='blue'
                />)
             }.bind(this))}

            </Layer>
            </Stage>
            <button onClick={(e) => (this.setState({showSectors: true}))}>Back</button>
        </div>
        }
        </div>
        </div>
      )
    }
}

_Map.propTypes = {
  colonies: PropTypes.array.isRequired,
  ships:    PropTypes.array,
  state:    PropTypes.object,
}

const mapStateToProps = (state) => ({state: state})
const mapDispatchToProps = (dispatch) => {
  return {
    onMapSelected: (id) => dispatch(mapSelected(id)),
  }
}

export const Map = connect(mapStateToProps, mapDispatchToProps)(_Map)
export {colony_coords, sector_coords, colony_subcoords}
