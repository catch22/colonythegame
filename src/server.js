var Gaussian = require('gaussian')

var AllMiners = [
  {key: 'atom',
   type: 'miner',
   name: 'Atom',
   price: 1000,
   production: [70, 20],
 },
 {key: 'vortex',
  type: 'miner',
  name: 'Vortex',
  price: 5000,
  production: [350, 100],
 },
 {key: 'acid',
  type: 'miner',
  name: 'Acid',
  price: 15000,
  production: [1500, 500],
},
]

var AllLanders = [
  {key: 'slug',
   type: 'lander',
   name: 'Slug',
   price: 100,
   speed: 0.1,
  },
  {key: 'vector',
   type: 'lander',
   name: 'Vector',
   price: 1000,
   speed: 1,
  },
  {key: 'impact',
  type: 'lander',
  name: 'Impact',
  price: 10000,
  speed: 1.2,
  },
  {key: 'quark',
  type: 'lander',
  name: 'Quark',
  price: 100000,
  speed: 3.5,
  },
]

export const AllShips = [...AllMiners, ...AllLanders]
export const AllShipsHash = AllShips.reduce((acc, ship) => {
  var keyhash = {}
  keyhash[ship.key] = Object.assign({}, ship)
  return Object.assign(keyhash, acc)
}, {})
var global_colony_id = 0

console.log(AllShipsHash)
/*

function create_colony(target) {
  var colony = {
    name: 'Unnamed',
    coords: target,
    population: 1,
    funding: 0,
    id:    global_colony_id++,
    lastTick: Date.now(),
  }
  return colony
}
*/

export function mine_ship(s) {
  var result = 0
  console.log('mine ship', s)
  var ship = find_ship(s.key)
  console.log('find ship returned', ship)
  if (ship) {
    if (!ship.distribution) {
      ship.distribution = Gaussian(ship.production[0], ship.production[1] * ship.production[1])
    }
    result = ship.distribution.ppf(Math.random())
    if (result < ship.production[0] - ship.production[1]) {
      result = ship.production[0] - ship.production[1]
    }
    result = Math.floor(result)
  } else {
    console.log("Error in mine_ship - ship not found", s)
  }
  console.log("mine_ship result", result)
  return result
}

export function find_ship(key, ships = null) {
  if (ships) {
    var found = ships.filter(s => (s.key === key))
    if (found.length > 0) {
      return found[0]
    }
  } else {
    return AllShipsHash[key]
  }
}
