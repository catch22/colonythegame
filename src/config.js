import * as Constants from './constants'

export const UPDATE_INTERVAL = 200
export const COLONY_EARNING = 10
export const MAP_WIDTH = 3000
export const MAP_HEIGHT = 3000
export const COLONY_UPDATE_PERIOD = 10000 // 1 hour
export const GOODS_UPDATE_PERIOD  = 10000 // 6 hours
export const CASH_UPDATE_PERIOD   = 10000 // 1 sec
export const INIT_BALANCE = 1500
export const USER_NAME = "AKDK's galactic empire";
export const USER_ID = "AKDK"
export const MINING_COOLDOWN = 3
export const PORT = process.env.API_PORT || 8082;
export const DEFAULT_EXPLORATION_DISTANCE = 5 //*60 // 5 mins

export var LANDER_COLORS = {}
LANDER_COLORS[Constants.STATE_IDLE] = 'green'
LANDER_COLORS[Constants.STATE_WAITING] = 'yellow'
LANDER_COLORS[Constants.STATE_EXPLORING] = 'red'
LANDER_COLORS[Constants.STATE_ARRIVED] = 'blue'
