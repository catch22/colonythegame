import fetch from 'isomorphic-fetch'
import * as Constants from './constants'

let global_id = 0
const API_SERVER = process.env.API_HOST || "localhost"
//const SERVER_BASE = "http://" + API_SERVER + ":" + Config.PORT + "/"
const SERVER_BASE = "http://192.168.1.101:8082/"

function request(url_params, callback, errhandler=error => {}) {
  //console.log("Requesting", url_params)
  fetch(SERVER_BASE + url_params)
  .then(response => response.json())
  .then(callback)
  .catch(error => {
    console.log("Request", url_params,"Error", error)
    errhandler()
  })
}

export function mineAll() {
  console.log("mineAll called")
  return dispatch => {
    request("mineall", json=>dispatch(json.action))
  }
}

export function fundAll(amount) {
  return dispatch => {
    request("fundall/" + amount, json=>dispatch(json.action))
  }
}

export function doInit() {
    return dispatch => {
      request("load/myself", json => {
        dispatch(doPoll(-1))
        return dispatch(loadState(json.state))
       })
    }
}

export function doPoll(last_seq) {
  return dispatch => {
    request("poll/"+last_seq, json => {
      var seq
      try {
        json.actions.forEach(a => {
          seq = a.seq
          dispatch(a)
        })
      } catch(e) {
        console.log("doPoll caught error", e)
      }
      dispatch(doPoll(seq))
    }, error => {
      dispatch(doPoll(seq))
    })
  }
}

export function doTick() {
  return {
    type: Constants.ACTION_TICK,
    now:  Date.now(),
  }
}

// Handle loading complete state
export function loadState(state) {
  return {
    type: Constants.ACTION_LOAD_STATE,
    state
  }
}

// Handle receiving the list of ships in the market
export function receiveShips(ships) {
  console.log("receiveShips", ships)
  return {
    type: Constants.ACTION_UPDATE_MARKET,
    ships
  }
}

// Handle receiving response for a buy request
function buyShipDelayed(ship) {
  console.log("buyShipDelayed called")
  return {
    type: Constants.ACTION_NEW_SHIP,
    ship: {
      ...ship,
      id: global_id++,
      count: 1,
    }
  }
}

// UI Callback to buy a ship
export function buyShip(ship) {
  console.log('buyship', ship)
  return dispatch => {
    request("buyship/" + ship.key, json=>dispatch(json.action))
  }
}

// UI Callback when a system is selected on the map
export function mapSelected(id) {
  return dispatch => {
    request("mapselect/" + id, json=>dispatch(json.action))
  }
}

export function landerExplore(ship) {
  return dispatch => {
    request('explore/' + ship.id, json=>dispatch(json.action))
  }
}

export function startColony(ship) {
  return dispatch => {
    request('startcolony/' + ship.id, json=>dispatch(json.action))
  }
}

export function colonyRename(colony, newname) {
  return dispatch => {
    // TODO: escape the URL
    request('renamecolony/' + colony.id + '/' + newname, json=>dispatch(json.action))
  }
}

export function colonyFund(colony, amount) {
  return dispatch => {
    // TODO: escape the URL
    request('fundcolony/' + colony.id + '/' + amount, json=>dispatch(json.action))
  }
}
