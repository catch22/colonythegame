import React from 'react';
import { connect } from 'react-redux'
import * as Common from './common'
import * as Map from './map'
import * as Config from './config'

import {colonyRename, colonyFund} from './actions'

var global_id = 1

// When this global points to a lander, that ship is waiting for map selection
var waitingForMapSelection = null

// -----------------------------------------
// Game logic for ships and ship related operations
//---------------------------------------------
class ColonyController {
  // Process tick for each colony

  // Process tick for all the colony
  tick() {
    var controller = this.controller
    this.ncoloniesUpdated = 0
    //var earnings = controller.state.earnings * (controller.ticks_sec)/60
    var colonies = controller.state.colonies.map(this.tickOne.bind(this))
    //controller.setState({balance: controller.state.balance + earnings})
    if (this.ncoloniesUpdated > 0) {
      controller.setState({colonies: colonies})
    }
  }

  marketTabs(state, props) {
    return {
      'Factories': <div>Factories here</div>
    }
  }

  assetTabs(state, props) {
    return {
      'Colonies': <AssetList colonies={state.colonies} state={state}/>,
    }
  }
  // Handle selection of colony for warping
  handleMapSelection(cid, a,b,c) {
    var controller = this.controller

    if (this.getShipWaitingForMapSelection()) {
      var coords = Map.colony_coords(cid)

      controller.startExploring(this.getShipWaitingForMapSelection(), coords)
      this.setShipWaitingForMapSelection(null)
    }
  }

  getShipWaitingForMapSelection() {
      return this.waitingForMapSelection
  }

  setShipWaitingForMapSelection(lander) {
    this.waitingForMapSelection = lander
  }
}

class ColonyOps extends React.PureComponent {
  constructor() {
    super();
  }

  render() {
    return (
      <span>
        <button onClick={(e) => (this.props.callback("rename", e))}>Rename</button>
        <button disabled={0 && this.props.balance < this.props.colony.population*10} onClick={(e) => (this.props.callback("fund", e))}>Fund</button>
      </span>
    )
  }
}

ColonyOps.propTypes = {
  callback: React.PropTypes.func,
};

class _Colony extends React.PureComponent {
  constructor() {
    super()
    //this.doColonyAction = this.doColonyAction.bind(this)
  }

  doColonyAction(colony, balance, action) {
    console.log("doColonyAction", action)
    if (action == 'rename') {
      var newname = prompt("Enter name for the colony: ")
      this.props.onColonyRename(colony, newname)
    } else if (action == 'fund') {
      var howmuch = prompt("How much ?")
      while (howmuch > balance) {
        howmuch = prompt("How much ?")
      }
      if (howmuch > 0) {
        this.props.onColonyFund(colony, howmuch)
      }
    } else {
      console.log("Unknown colony action", action)
    }
  }


  render() {
    let {colony} = this.props
    return (
      <tr>
        <td>
          {this.props.colony.name}<br/>
          <div className="coords">{/*this.props.colony.coords[0]},{this.props.colony.coords[1]*/}
            {Common.GOODS_LIST[colony.goods[0]]},
            {Common.GOODS_LIST[colony.goods[1]]}
          </div>
        </td>
        <td>
          {this.props.colony.population}
        </td>
        <td>
          {Math.round(10.0*this.props.colony.funding/this.props.colony.population)/10}x
        </td>
        <td>
         <ColonyOps balance={this.props.balance}
            callback={this.doColonyAction.bind(this, this.props.colony, this.props.balance)}
            colony={this.props.colony}/>
        </td>
      </tr>)
  }
}

_Colony.propTypes = {
  callback: React.PropTypes.func,
};

class AssetList extends React.PureComponent {
  render() {
    let {state, colonies} = this.props
    return (
      <table width="100%"><tbody>
      <tr><th width="40%"/><th width="30%"/><th width="30%"/></tr>
      <tr><td colSpan="3" className="subheader">Colonies</td></tr>
      {colonies.map((colony) => (
        <Colony balance={state.balance}
                       key={colony.coords.join()}
                       colony={colony}/>
        )
      )}
      </tbody></table>
    )
  }
}
AssetList.propTypes = {
  colonies: React.PropTypes.array.isRequired,
  state: React.PropTypes.object.isRequired,
};
// Global ship controller singleton
export var colonyController = null;
export function init(controller) {
  if (!colonyController) {
    colonyController = new ColonyController(controller)
  }
  return colonyController
}

const mapStateToProps = (state) => ({state: state})
const mapDispatchToProps = (dispatch) => {
  return {
    onColonyRename: (colony, newname) => dispatch(colonyRename(colony, newname)),
    onColonyFund: (colony, amount) => dispatch(colonyFund(colony, amount)),
  }
}
export const Colony = connect(mapStateToProps, mapDispatchToProps)(_Colony)

export {AssetList}
