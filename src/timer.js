import React from 'react';
import { connect } from 'react-redux'
import { doTick, doInit } from './actions'

class _Timer extends React.Component {
  componentDidMount() {
    this.props.doInit()
    //this.interval = setInterval(this.forceUpdate.bind(this), this.props.updateInterval || 200)
  }

  render() {
    this.props.doTick()
    return (
      <div>Timer!</div>
    )
  }
}

export const Timer = connect((state) => (state), {doTick, doInit})(_Timer)
