import React from 'react';
import Progress from 'react-progressbar';
import { connect } from 'react-redux'
import Gaussian from 'gaussian'

import * as Common from './common'
import * as Colony from './colony'
import * as Map from './map'
import * as Constants from './constants'
import * as Config from './config'

import {landerExplore, startColony} from './actions'

var global_id = 1

// -----------------------------------------
// Game logic for ships and ship related operations
//---------------------------------------------
class ShipController {
  // Update lander position based on progress
  updateProgress(ship) {
    var scale = ship.progress/ship.distance
    ship.position = ship.target.map((n) => (scale*n))
  }

  marketTabs(state, props) {
    return {
      'Miners': <MarketList ships={state.market.ships.filter(s => (s.type === 'miner'))} state={state} onBuyShip={props.onBuyShip}/>,
      'Landers': <MarketList ships={state.market.ships.filter(s => (s.type === 'lander'))} state={state} onBuyShip={props.onBuyShip}/>,
    }
  }

  assetTabs(state, props) {
    return {
      'Ships': <AssetList ships={state.ships} state={state}/>,
    }
  }
}

//----------------------------------------------
// UI Handlers
//----------------------------------------------


// Render one ship in the market list and allow buying
class MarketShip extends React.PureComponent {
  render() {
    let {disabled, ship, state, onBuyShip} = this.props
    return(<div><button disabled={disabled} onClick={() => onBuyShip(ship)}>Buy {ship.name} for {ship.price}</button></div>)
  }
}

MarketShip.propTypes = {
  disabled: React.PropTypes.bool,
  ship:     React.PropTypes.object.isRequired,
  state:    React.PropTypes.object.isRequired,
  onBuyShip:    React.PropTypes.func.isRequired,
};

// Render a list of ships in the market panel
class MarketList extends React.PureComponent {
  render() {
    let {ships, state, onBuyShip} = this.props
    return (<div>
      {ships.map((ship) => (
      <MarketShip
       key={ship.key}
       state={state}
       disabled={ship.price > state.balance}
       onBuyShip={onBuyShip}
       /*buyCallback={controller.BuyShip.bind(controller,ship)}*/
       ship={ship}/>)
    )}
    </div>)
  }
}

MarketList.propTypes = {
  state: React.PropTypes.object.isRequired,
  onBuyShip: React.PropTypes.func.isRequired,
  ships: React.PropTypes.array.isRequired,
};

// Render operations for a mining ship in ship asset list
class MinerOps extends React.PureComponent {
  constructor() {
    super();
  }

  render() {
    return (
      <div></div>
    )
  }
}

MinerOps.propTypes = {
  callback: React.PropTypes.func,
};

// Render operations for a lander ship in ship asset list
class LanderOps extends React.PureComponent {
  constructor() {
    super();
  }

  render() {
    return (
      <span>
        {this.props.ship.state == Constants.STATE_IDLE && <span>is idle <button onClick={(e) => (this.props.callback(this.props.ship, "explore", e))}>Explore</button></span>}
        {this.props.ship.state == Constants.STATE_WAITING && <span>waiting for user</span>}
        {this.props.ship.state == Constants.STATE_EXPLORING && <span>is warping to {this.props.ship.target.join()} <Progress completed={100.0*this.props.ship.progress/this.props.ship.distance} /></span>}
        {this.props.ship.state == Constants.STATE_ARRIVED && <span>arrived at {this.props.ship.target.join()} ! <button onClick={(e) => (this.props.callback(this.props.ship, "start", e))}>Start Colony</button></span>}

      </span>
    )
  }
}

LanderOps.propTypes = {
  callback: React.PropTypes.func,
};

// Render one ship in the asset list and allow ship operations
class _Ship extends React.PureComponent {
    constructor() {
      super();
      this.onEvent = this.onEvent.bind(this)
    }

    getShipProgress(ship) {
      return Math.round(100.0*ship.progress / ship.period)
    }

    onEvent(ship, action) {
      console.log("Ship.onEvent", action)
      switch (action) {
        case 'explore':
          this.props.onLanderExplore(ship)
          break
        case 'start':
          this.props.onStartColony(ship)
          break
      }
    }

    render() {
      let {state, ship} = this.props
      return (
        <tr>
          <td>{ship.count} x {ship.name}</td>
          <td colSpan="2">
            {ship.type == 'miner' && <MinerOps callback={this.onEvent} ship={ship}/>}
            {ship.type == 'lander' && <LanderOps callback={this.onEvent} ship={ship}/>}
          </td>
        </tr>)
    }
}
_Ship.propTypes = {
  state:    React.PropTypes.object.isRequired,
  ship:     React.PropTypes.object.isRequired,
};

// Render list of ships in the asset panel
class AssetList extends React.PureComponent {
  // Handle ship action from the asset list
  HandleShipAction(ship, action, e) {
    var controller = this.props.controller
    if (ship.type == 'lander') {
      if (ship.state == Constants.STATE_IDLE && action == 'explore' && Colony.colonyController.getShipWaitingForMapSelection() == null) {
        Colony.colonyController.setShipWaitingForMapSelection(ship)
        ship.state = Constants.STATE_WAITING

        //this.startExploring(ship, [100,100])
        console.log("Waiting for map for ship", ship.id, ship.name, ship.state)
      } else if (ship.state == Constants.STATE_ARRIVED && action == 'start') {
        Colony.colonyController.doStartColony(ship)
        ship.state = Constants.STATE_IDLE
      }
      //this.setState({ships: this.state.ships})
      this.props.controller.updateShip(ship)
    }
  }

  render() {
    let {ships, state, event} = this.props
    return (
    <table width="100%"><tbody>
      <tr><th width="40%"/><th width="30%"/><th width="30%"/></tr>

      <tr><td colSpan="3" className="subheader">Miners</td></tr>
        {ships.filter((ship) => (ship.type == 'miner'))
          .map((ship) => (
            <Ship key={ship.id} ship={ship} state={state}/>
          ))}

      <tr><td colSpan="3" className="subheader">Landers</td></tr>
      {ships.filter((ship) => (ship.type == 'lander'))
        .map((ship) => (
          <Ship key={ship.id} ship={ship} state={state}/>
        ))}

      <tr><td colSpan="3" className="subheader">Transporters</td></tr>
      {ships.filter((ship) => (ship.type == 'transport'))
        .map((ship) => (
          /*callback={this.HandleShipAction.bind(this)}*/
          <Ship key={ship.id} ship={ship} state={state}/>
        ))}

    </tbody></table>)
  }
}

AssetList.propTypes = {
  ships: React.PropTypes.array,
  state:    React.PropTypes.object.isRequired,
}

// Global ship controller singleton
export var shipController = null
export function init() {
  if (!shipController) {
    shipController = new ShipController()
  }
  return shipController
}

const mapStateToProps = (state) => ({state: state})
const mapDispatchToProps = (dispatch) => {
  return {
    onLanderExplore: (ship) => dispatch(landerExplore(ship)),
    onStartColony: (ship) => dispatch(startColony(ship)),

  }
}
export const Ship = connect(mapStateToProps, mapDispatchToProps)(_Ship)

export {MarketShip, MarketList, AssetList, clone, newship}
