import * as Colony from './colony'
import * as Empire from './empire'
import * as Ship from './ship'
import * as Constants from './constants'

var global_id = 0
export function ship_reducer(state, event) {
  // Handle single event
  if (event.type !== Constants.ACTION_TICK) {
    console.log('reducer', event)
  }
  let ship = event.ship
  switch (event.type) {
    case Constants.ACTION_MULTI:
      console.log('ship_reducer multi actions', event.actions)
      return event.actions.reduce((st, action) => (ship_reducer(st, action)), state)
      break;

    case  Constants.ACTION_NEW_SHIP:
      return {
        ...state,
        balance: event.balance,
        ships: state.ships.concat({
        ...ship,
        })
      }
      break

    case Constants.ACTION_UPDATE_SHIPS:
      return {
        ...state,
        balance: event.balance,
        ships: event.ships
      }
      break

    case Constants.ACTION_UPDATE_WALLET:
      return {
        ...state,
        balance: event.balance,
      }
      break

    case Constants.ACTION_UPDATE_COLONY:
      return {
        ...state,
        balance: event.balance,
        colonies: event.colonies,
      }
      break

    case Constants.ACTION_UPDATE_GOODS:
      return {
        ...state,
        balance: event.balance,
        goods: event.goods,
      }
    case Constants.ACTION_LOAD_STATE:
      console.log("Loaded state", event.state)
      return event.state

    case Constants.ACTION_MINE:
      return {
        ...state,
        balance: event.balance,
        newBalance: event.newBalance,
        miningCooldown: event.miningCooldown,
      }
      break

    case Constants.ACTION_MINE_PROGRESS:
      return {
        ...state,
        miningCooldown: event.miningCooldown,
      }
      break
    case Constants.ACTION_UPDATE_MARKET:
      return {
        ...state,
        market: {
          ships: event.ships,
        }
      }
/*
    case Constants.ACTION_TICK:
      if (state.market.loaded = false) {

      }
      return state
      */
  }
  return state
}
