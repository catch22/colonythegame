import React from 'react';
import { connect } from 'react-redux'
import Progress from 'react-progressbar';
import Gaussian from 'gaussian'
import * as Common from './common'
import * as Colony from './colony'

// -----------------------------------------
// Game logic for ships and ship related operations
//---------------------------------------------
class GoodsController {
  constructor() {
  }

  marketTabs(state, props) {
    return {
    }
  }

  assetTabs(state, props) {
    //console.log(state)
    return {
      'Goods': <AssetList state={state} goods={state.goods} />,
    }
  }
}

class _AssetList extends React.PureComponent {
  render() {
    let {state, goods} = this.props
    return (<div>List of my Goods
      {goods.map(g => (<div key={g.name}>{g.name} : {g.count}</div>))}
      </div>)
  }
}
_AssetList.PropTypes = {
  state: React.PropTypes.object.isRequired,
  goods: React.PropTypes.object.isRequired,
}

// Global ship controller singleton
export var goodsController = null
export function init() {
  if (!goodsController) {
    goodsController = new GoodsController()
  }
  return goodsController
}

const mapStateToProps = (state) => ({state: state})
const mapDispatchToProps = (dispatch) => {
  return {
    // onFunc: params => dispatch(func(params))
  }
}
export const AssetList = connect(mapStateToProps, mapDispatchToProps)(_AssetList)
