import fetch from 'isomorphic-fetch'

export class Poller {
  constructor(base_url) {
    this.last_seq = -1
    this.base_url = base_url
  }

  donext() {
    var url = this.base_url + this.last_seq
    //console.log("Fetching", url)
    fetch(url)
    .then(response => response.json())
    .then(json => {
      console.log(json)
      this.last_seq = json.actions[json.actions.length-1].seq
      setTimeout(() => {this.donext()}, 100)
    })
    .catch(reason => {
      console.log("Error:", reason)
      setTimeout(() => {this.donext()},100)
    })
  }
}
