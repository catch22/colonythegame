import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux'
// Progress bar
import Progress from 'react-progressbar';

// Tabbed interface
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import { Timer } from './timer'

// App modules
import * as Common from './common'
import * as Ship from './ship'
import * as Colony from './colony'
import * as Map from './map'
import * as Goods from './goods'
import * as Config from './config'

import { buyShip, mineAll, fundAll } from './actions'

var modules = []

class _Empire extends React.PureComponent {
  constructor() {
    super();
    modules.push(Ship.init(this))
    modules.push(Colony.init(this))
    modules.push(Goods.init(this))
    this.lastTick = Date.now();
  }

  componentDidMount() {
    return
    this.timerID = setInterval(
      () => this.Tick(),
      Config.UPDATE_INTERVAL
    );
    console.log("Started mount")
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  // ----------------------------
  // Game State Accessors
  // ----------------------------


  // Main view for the app
  render() {
    let {state, onMineAll, onFundAll} = this.props
    let props = this.props
    //console.log('render state', state)
    return (
    <div>
      <h1>Welcome to Colony!</h1>
      <h3>{state.name}</h3>

      <div className="ColonyInfo">
        <p>Hello {state.user}</p>
        <div><b>Balance:</b> ${Math.round(state.balance)}</div>
        <div><b>Number of colonies:</b> {state.colonies.length}</div>
      </div>

      <div className="ColonyMain">
      <table width="100%"><tbody>
        <tr>
          <th width="300px"/>
          <th/>
          <th width="300px"/>
        </tr>

        <tr>
          <td>
            <div className="ColonyMarket">
              <Tabs forceRenderTabPanel={true}>
                <TabList>
                {modules.map(function(m) {
                  if (typeof(m.marketTabs) == 'function') {
                    var tabs = m.marketTabs(state, props)
                    return Object.keys(tabs).map(function(tabname, index) {
                      return (<Tab>{tabname}</Tab>)
                    })
                  }
                })}
                </TabList>

                {modules.map(function(m) {
                  if (typeof(m.marketTabs) == 'function') {
                    var tabs = m.marketTabs(state, props)
                    return Object.keys(tabs).map(function(tabname, index) {
                      return (<TabPanel>{tabs[tabname]}</TabPanel>)
                    })
                  }

                })}
              </Tabs>
            </div>
          </td>

          <td>
            <div className="ColonyDashboard">

              {/*callback={Colony.colonyController.handleMapSelection.bind(Colony.colonyController)}*/}
              <Map.Map colonies={state.colonies} ships={state.ships} state={state}/>

              <div>{state.miningCooldown}</div>
              <div className={(state.miningCooldown == 0)?'hidden':''}>
                <div className="mining-progress"><Progress completed={100.0*(Config.MINING_COOLDOWN-state.miningCooldown)/Config.MINING_COOLDOWN}/></div>
              </div>
              <div><span>
                <button disabled={state.miningCooldown > 0} onClick={e => onMineAll()}>Mine</button>
              </span>
              <span>
                <button onClick={e => {
                  var ask = () => prompt("How much ?")
                  var amount = ask()
                  while (amount > state.balance) {
                    amount = ask()
                  }
                  onFundAll(amount)
                }}>Fund All Colonies</button>
              </span>
              </div>
              <div className={(!state.miningCooldown)?'hidden':''}>You earned {state.newBalance} dollars last mining action</div>
            </div>
          </td>

          <td>
            <div className="ColonyAssets">
              <Tabs forceRenderTabPanel={true}>
                <TabList>
                {modules.map(function(m) {
                  if (typeof(m.assetTabs) == 'function') {
                    var tabs = m.assetTabs(state, props)
                    return Object.keys(tabs).map(function(tabname, index) {
                      return (<Tab>{tabname}</Tab>)
                    })
                  }
                })}
                </TabList>

                {modules.map(function(m) {
                  if (typeof(m.assetTabs) == 'function') {
                    var tabs = m.assetTabs(state, props)
                    return Object.keys(tabs).map(function(tabname, index) {
                      return (<TabPanel>{tabs[tabname]}</TabPanel>)
                    })
                  }

                })}
              </Tabs>
            </div>
          </td>
        </tr>
      </tbody></table>
      </div>
      <Timer updateInterval="1000" />
    </div>
  )
  }
}

const mapStateToProps = (state) => ({state: state})
const mapDispatchToProps = (dispatch) => {
  return {
    onBuyShip: (ship) => dispatch(buyShip(ship)),
    onMineAll: () => dispatch(mineAll()),
    onFundAll: (amount) => dispatch(fundAll(amount)),
    }
  }

export const Empire = connect(mapStateToProps, mapDispatchToProps)(_Empire)
