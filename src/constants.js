/* Events */
export const EVENT_MINE_ALL= "event-mine-all"
export const EVENT_FUND_ALL= "event-fund-all"
export const EVENT_FUND=     "event-fund"
export const EVENT_BUY_SHIP= "event-ship-buy"
export const EVENT_EXPLORE=  "event-ship-lander-explore"
export const EVENT_START_COLONY=  "event-ship-start-colony"

export const ACTION_LOAD_STATE=   "action-load-state"
export const ACTION_TICK=         "action-tick"
export const ACTION_NEW_SHIP=      "action-ship-new"
export const ACTION_NEW_COLONY=    "action-colony-new"
export const ACTION_UPDATE_SHIPS=   "action-ship-update"
export const ACTION_UPDATE_COLONY= "action-colony-update"
export const ACTION_UPDATE_WALLET= "action-update-wallet"
export const ACTION_MINE=          "action-mine"
export const ACTION_MINE_PROGRESS= "action-mine-progress"
export const ACTION_FUND=          "action-fund"
export const ACTION_UPDATE_MARKET= "action-update-market"
export const ACTION_NOOP=          "action-noop"
export const ACTION_MULTI=         "action-multi"
export const ACTION_UPDATE_GOODS=  "action-goods-update"

export const STATE_IDLE = 'state-idle'
export const STATE_WAITING = 'state-waiting'
export const STATE_ARRIVED = 'state-arrived'
export const STATE_EXPLORING = 'state-exploring'
