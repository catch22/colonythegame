import * as fs from 'fs'
import * as Common from './common'
import * as Constants from './constants'
import * as Config from './config'
import * as Logic from './logic'
import * as Server from './server'

const Game = Logic.Game
const STATE_FILE = "gamestate.json"
var restify = require('restify');

var global_ship_id = 0
var global_test = 0

function set_route(srv, route, handler) {
  srv.get(route, handler);
  srv.head(route, handler);
}

var STATES = {

}

var init_state = {
  name: Config.USER_NAME,
  user: Config.USER_ID,
  balance: Config.INIT_BALANCE,
  newBalance: 0,
  ships: [],
  colonies: [],
  market: {
    ships: Server.AllShips,
  },
  miningCooldown: 0,
  earnings: 0,
  goods: [],
  api: {
    lastTick: Date.now(),
    actions: [],
    waiter: null,
    coloniesTick: 0,
    goodsTick: 0,
    cashTick: 0,
  }
}

var STATE = {}

function save_state() {
  var state = Object.assign({}, STATE)
  state.market = {}
  state.api = Object.assign({}, init_state.api)
  var result
  try {
    result = fs.writeFileSync(STATE_FILE, JSON.stringify(state), 'utf8');
    if (result) {
      console.log('save_state', result)
    }
  } catch(e) {
    console.log("--------- JSON.stringify error ---------------")
    console.log(e)
    console.log(state)
    console.log("------------------------")
  }
}

function sendUpdate(state, action) {
  action.seq = global_test++
  state.api.actions.push(action)
  console.log('sendUpdate', action,
    'waiter', (state.api.waiter === null)?"not present":('last '+state.api.waiter.last),
    'actions', (state.api.actions.length>0)?(state.api.actions[0].seq+'-'+state.api.actions[state.api.actions.length-1].seq):'none')
  if (state.api.waiter) {
    state.api.actions = state.api.actions.filter(a => (a.seq > state.api.waiter.last))

    if (state.api.actions.length > 0) {
      state.api.waiter.res.send({actions: state.api.actions })
      state.api.waiter = null
    } else {
      state.api.actions.push(action)
    }
  }
}

function sendAction(res, action) {
  return res.send({action: action})
}

function sendActions(res, actions) {
  return res.send({action: {
    type: Constants.ACTION_MULTI,
    actions
  }})
}

function sendNoop(res) {
  sendAction(res, {type: Constants.ACTION_NOOP})
}

function send_update_colonies(res, state) {
  sendAction(res, {
    type: Constants.ACTION_UPDATE_COLONY,
    colonies: state.colonies,
    balance: state.balance,
  })
}

// For testing
setInterval(() => {
  var now = Date.now()
  var ticks = now-STATE.api.lastTick
  var actions = Game.tick(STATE, ticks)
  if (actions.length == 1) {
    sendUpdate(STATE, actions[0])
  } else if (actions.length > 1) {
    sendUpdate(STATE, {
      type: Constants.ACTION_MULTI,
      actions
    })
  }
  STATE.api.lastTick = now
  save_state()
}, 1000)

process.on('SIGINT', function() {
    console.log("Caught interrupt signal");

    process.exit();
});

function finish(next) {
  next()
  save_state()
}

/*
 * API Handlers
 */
function doPoll(req, res, next) {
  var last = req.params.last
  var state = STATE
  if (state.api.actions.length > 0) {
    state.api.actions = state.api.actions.filter(a => (a.seq > last))
  }

  if (state.api.actions.length > 0) {
    res.send({actions: state.api.actions})
    state.api.actions = []
  } else {
    state.api.waiter = {req, res, last}
  }
  finish(next)
}

function getShips(req, res, next) {
  res.send({ships: Server.AllShips});
  finish(next);
}

function loadState(req, res, next) {
  res.send({state: Object.assign({},STATE, {api:null})})
  finish(next)
}

function mineAll(req, res, next) {
  console.log('mineAll called')
  var amount = STATE.ships.filter(s => (s.type === 'miner')).reduce((sum,ship) => (sum+Server.mine_ship(ship)*ship.count), 0)

  // DO TICK PROCESSING
  STATE.api.lastTick = Date.now()
  STATE.balance += amount
  STATE.newBalance = amount
  STATE.miningCooldown = Config.MINING_COOLDOWN

  sendActions(res, [{
      type: Constants.ACTION_MINE,
      newBalance: STATE.newBalance,
      balance: STATE.balance,
      miningCooldown: STATE.miningCooldown,
    }]
  )
}

function fundAll(req, res, next) {

}

function buyShip(req, res, next) {
  var ship = Server.find_ship(req.params.key)
  if (!ship || ship.price > STATE.balance) {
    res.send({
      type:"action-error",
      message: "You do not have enough cash to buy this ship"
    })
    return
  }

  // If this is a miner and we already own atleast one, increment count
  var foundship = null
  if (ship.type === 'miner') {
    foundship = Server.find_ship(ship.key, STATE.ships)
    if (foundship) {
      foundship.count++
    }
  }
  if (!foundship) {
    // We need to add a new ship
    foundship = Object.assign({state: Constants.STATE_IDLE, origin: [0,0]}, ship, {count:1, id: ++global_ship_id})
    STATE.ships.push(foundship)
  }

  // Update balance
  STATE.balance -= ship.price

  sendAction(res, {
      type: Constants.ACTION_UPDATE_SHIPS,
      ships: STATE.ships,
      balance: STATE.balance
    })
}

function landerExplore(req, res, next) {
  var state = STATE
  var landers = state.ships.filter(
    s => (s.type === 'lander' &&
          s.state === Constants.STATE_IDLE &&
          ''+s.id === req.params.shipid))
  if (landers.length > 0) {
    var lander = landers[0]
    lander.state = Constants.STATE_WAITING
    res.send({
      action: {
        type: Constants.ACTION_UPDATE_SHIPS,
        ships: state.ships,
        balance: state.balance,
      }
    })
  } else {
    sendAction(res, {type: Constants.ACTION_NOOP})
  }
}

function mapSelect(req, res, next) {
  var state = STATE
  var landers = state.ships.filter(
    s => (s.type === 'lander' &&
          s.state === Constants.STATE_WAITING))
  if (landers.length > 0) {
    //landers[0].state = Constants.STATE_EXPLORING
    Game.lander_explore(landers[0], req.params.id)
    sendAction(res, {
      type: Constants.ACTION_UPDATE_SHIPS,
      ships: state.ships,
      balance: state.balance,
    })
  } else {
    sendNoop(res)
  }
}

function startColony(req, res, next) {
  var state = STATE
  var landers = state.ships.filter(
    s => (s.type === 'lander' &&
          s.id+'' === req.params.id &&
          s.state === Constants.STATE_ARRIVED))
  if (landers.length > 0) {
    var ship = landers[0]
    Game.start_colony(state,ship)
    var actions = [{
      type: Constants.ACTION_UPDATE_COLONY,
      colonies: state.colonies,
      balance: state.balance,
    },{
      type: Constants.ACTION_UPDATE_SHIPS,
      ships: state.ships,
      balance: state.balance,
    }]

    sendActions(res, actions)
  } else {
    sendNoop(res)
  }
}

function renameColony(req, res, next) {
  var state = STATE
  var colonies = state.colonies.filter(
    c => (c.id+'' === req.params.id))
  if (colonies.length > 0) {
    var colony = colonies[0]
    colony.name = req.params.name
    send_update_colonies(res, state)
  } else {
    sendNoop(res)
  }
}

function fundColony(req, res, next) {
  var state = STATE
  var amount = Math.floor(req.params.amount)
  var colonies = state.colonies.filter(
    c => (c.id+'' === req.params.id))
  if (colonies.length > 0 && amount <= state.balance) {
    var colony = colonies[0]
    colony.funding += amount
    state.balance -= amount
    send_update_colonies(res, state)
  } else {
    sendNoop(res)
  }
}

var server = restify.createServer();
server.use(
  function crossOrigin(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    return next();
  }
)
server.get('/ships/', getShips);
server.head('/ships/', getShips);
server.get('/load/:user', loadState);
server.head('/load/:user', loadState);
server.get('/buyship/:key', buyShip);
server.head('/buyship/:key', buyShip);
server.get('/mineall', mineAll);
server.head('/mineall', mineAll);
server.get('/poll/:last', doPoll);
server.head('/poll/:last', doPoll);
set_route(server, 'explore/:shipid', landerExplore)
set_route(server, 'mapselect/:id', mapSelect)
set_route(server, 'startcolony/:id', startColony)
set_route(server, 'renamecolony/:id/:name', renameColony)
set_route(server, 'fundcolony/:id/:amount', fundColony)

fs.readFile(STATE_FILE, 'utf8', function (err, data) {
  if (err) {
    STATE = Object.assign({}, init_state)
  } else {
    STATE = JSON.parse(data);
  }
  STATE.market = Object.assign({}, init_state.market)
  STATE.ships.forEach(s => {
    if (s.id > global_ship_id) {
      global_ship_id = s.id+1
    }
  })

  STATE.colonies.forEach(c => {
    if (c.id > Game.colony_id) {
      Game.colony_id = c.id+1
    }
  })

  STATE.api = Object.assign({}, init_state.api)
  console.log("Loaded state", STATE, 'ship id', global_ship_id, 'colony id', Game.colony_id)
  server.listen(Config.PORT, function() {
    console.log('%s listening at %s', server.name, server.url);
  });
})
