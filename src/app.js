// ----------------------
// IMPORTS

// React
import React from 'react';
import PropTypes from 'prop-types';

import {combineReducers} from 'redux'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'

import './styles.global.css';
import css from './styles.css';
import sass from './styles.scss';
import less from './styles.less';

import * as Common from './common'
import * as Colony from './colony'
import * as Empire from './empire'
import * as reducers from './reducers'

function reducer(state, action) {
  console.log("reducer", state, action)
  return state
}

function configureStore(preloadedState) {
  return createStore(
    reducers.ship_reducer,
    preloadedState,
    applyMiddleware(
      thunkMiddleware,
    )
  )
}

const store = configureStore({
  name: '',
  user: '',
  balance: '',
  newBalance: 0,
  ships: [],
  colonies: [],
  market: {
    ships: [],
  },
  goods: [],
  miningCooldown: 0,
  earnings: 0,
})

export default () => (
  <Provider store={store}>
    <h2>This is Mr Blobbo</h2>
    <div>
    <Empire.Empire />
    </div>
  </Provider>
);
