import * as Config from './config'

export function range(start, count) {
  return Array.apply(0, Array(count))
    .map(function (element, index) {
      return index + start;
  });
}

export function random(from, to) {
  var r = Math.floor((to+1-from) * Math.random() + from)
  return r
}

export function lander_color(ship) {
  var color = 'black'
  try {
    color = Config.LANDER_COLORS[ship.state]
  } catch(e) {
    console.log("lander_color error", e)
  }
  return color
}
