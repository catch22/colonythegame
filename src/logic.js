import * as Common from './common'
import * as Constants from './constants'
import * as Config from './config'
import * as Utils from './utils'

export const STAGE_WIDTH = 700  // Pixel width of the map
export const STAGE_HEIGHT = 700 // Pixel height of the map
export const STAGE_PADDING = 50 // Padding in all directions
export const SECTOR_WIDTH = 10  // Number of sectors in a grid row
export const SECTOR_HEIGHT = 10 // Number of sectors in a grid column
export const SECTOR_SQUARE = 15 // Length of the sector square sides
export const COLONY_WIDTH = 25  // Number of colonies in a grid row
export const COLONY_HEIGHT = 25 // Number of colonies in a grid column

function incr_goods(goods, gid, count) {
  var goods_found = false
  goods.forEach((g) => {
    if (g.id === gid) {
      g.count += count
      goods_found = true
    }
  })

  if (!goods_found) {
    goods.push({
      id: gid,
      name: Common.GOODS_LIST[gid],
      count: count,
    })
  }
}

function calc_colony_incr(colony) {
  var inc = 0
  if (colony.population < 10) {
    inc = 1
  } else if (colony.population < 100) {
    inc = 2
  } else if (colony.population < 1000) {
    inc = 3
  } else if (colony.population < 10000) {
    inc = 4
  } else if (colony.population < 100000) {
    inc = 5
  } else {
    inc = 10
  }
  return inc
}

function sec_distance(a, b) {
  return Config.DEFAULT_EXPLORATION_DISTANCE *
          Math.sqrt((b[0] - a[0])*(b[0] - a[0]) +
                    (b[1] - a[1])*(b[1] - a[1]))
}

class Logic {
  constructor() {
    this.shipStateChanged = false
    this.colony_id = 0 // Unique ID for colonies
  }

  // Start exploration from a lander to a given colony id
  lander_explore(ship, cid) {
    var target = colony_coords(cid)
    ship.state = Constants.STATE_EXPLORING
    ship.progress = 0.1
    ship.target = target
    ship.target_sector = sector_coords(target[0])
    ship.distance = sec_distance(ship.origin, ship.target_sector) / ship.speed //Config.DEFAULT_EXPLORATION_DISTANCE
    console.log('lander_explore ship.origin', ship.origin, 'target_sector', ship.target_sector, 'distance', ship.distance)
  }

  start_colony(state, ship) {
    var colony = this.create_colony(ship.target)
    state.colonies.push(colony)
    ship.origin = ship.target_sector
    ship.progress = 0
    delete(ship.distance)
    delete(ship.target)
    delete(ship.target_sector)
    ship.state = Constants.STATE_IDLE
    console.log('start_colony ship.origin', ship.origin)
  }
  // Update mine cooldown
  mine_tick(state, ticks) {
    if (state.miningCooldown > 0) {
      state.miningCooldown -= ticks/1000.0
      if (state.miningCooldown < 0) {
        state.miningCooldown = 0
      }
      return [{
          type:Constants.ACTION_MINE_PROGRESS,
          miningCooldown: state.miningCooldown
        },
      ]
    }
    return []
  }

  // Update ship state each Tick
  ship_tick(ship, ticks) {
    if (ship.type === 'lander' && ship.state === Constants.STATE_EXPLORING) {
      if (!ship.target_sector) {
        ship.target_sector = sector_coords(ship.target[0])
      }
      ship.progress += ticks/1000.0
      if (ship.progress >= ship.distance) {
        ship.progress = ship.distance
        ship.state = Constants.STATE_ARRIVED
      }

      var scale = (ship.distance>0)?(ship.progress / ship.distance):0
      ship.position_sector = ship.target_sector.map((n,i) => ( n * scale + ship.origin[i] * (1-scale) ) )
      console.log('ship_tick scale', scale, 'origin', ship.origin, 'target_sector', ship.target_sector, 'pos_sector', ship.position_sector)

      this.shipStateChanged = true
    }
    return ship
  }

  // Handle ticks for all ships
  ships_tick(state, ticks) {
    //this.mineTick()
    this.shipStateChanged = false
    var ships = state.ships.map(s => this.ship_tick(s, ticks))
    if (this.shipStateChanged) {
      state.ships = ships
      return [{
          type:Constants.ACTION_UPDATE_SHIPS,
          ships: state.ships,
          balance: state.balance,
        },
      ]
    }
    return []
  }

  colony_tick(state, colony, nperiods, nperiod_goods, nperiod_cash) {
    // Update population
    var popinc = 0
    if (nperiods > 0) {
      if (colony.funding > 50*colony.population) {
        popinc = Utils.random(26,100)
        colony.funding -= 10*colony.population
      } else if (colony.funding > 10*colony.population) {
        popinc = Utils.random(6,25)
        colony.funding -= 2*colony.population
      } else if (colony.funding > 50) {
        popinc = Utils.random(1,5)
      } else {
        popinc = Utils.random(1,2)
      }
      colony.population += popinc*nperiods
      this.ncoloniesUpdated += 1

    }

    // Update colony goods production
    nperiods = nperiod_goods
    if (nperiods > 0) {
      var goodsinc = calc_colony_incr(colony)
      incr_goods(state.goods, colony.goods[0], goodsinc*nperiods)
      incr_goods(state.goods, colony.goods[1], goodsinc*nperiods)
      this.goodsUpdated = true
    }

    nperiods = nperiod_cash
    if (nperiods > 0) {
      this.cashIncrement = calc_colony_incr(colony)
      state.balance += this.cashIncrement
    }
    return colony
  }

  // Handle ticks for all ships
  colonies_tick(state, ticks) {
    var actions = []
    state.api.coloniesTick += ticks
    state.api.goodsTick += ticks
    state.api.cashTick += ticks

    var nperiods = Math.floor(state.api.coloniesTick / Config.COLONY_UPDATE_PERIOD)
    var nperiod_goods = Math.floor(state.api.goodsTick / Config.GOODS_UPDATE_PERIOD)
    var nperiod_cash = Math.floor(state.api.cashTick / Config.CASH_UPDATE_PERIOD)

    this.ncoloniesUpdated = 0
    this.goodsUpdated = false
    this.cashIncrement = 0

    var colonies = state.colonies.map(
      c => this.colony_tick(state, c, nperiods, nperiod_goods, nperiod_cash))
    if (this.ncoloniesUpdated > 0) {
      state.colonies = colonies
      actions.push({
        type:Constants.ACTION_UPDATE_COLONY,
        colonies: state.colonies,
        balance: state.balance,
      })
    }

    if (this.goodsUpdated > 0) {
      actions.push({
        type: Constants.ACTION_UPDATE_GOODS,
        goods: state.goods,
        balance: state.balance,
      })
    }

    if (this.cashIncrement > 0 && !this.goodsUpdated && !this.ncoloniesUpdated) {
      actions.push({
        type: Constants.ACTION_UPDATE_WALLET,
        balance: state.balance,
      })
    }
    if (nperiods > 0) {
        state.api.coloniesTick -= nperiods * Config.COLONY_UPDATE_PERIOD
    }

    if (nperiod_goods > 0) {
      state.api.goodsTick -= nperiod_goods * Config.GOODS_UPDATE_PERIOD
    }

    if (nperiod_cash > 0) {
      state.api.cashTick -= nperiod_cash * Config.CASH_UPDATE_PERIOD
    }

    return actions
  }

  tick(state, ticks) {
    var actions = []
    var result = this.ships_tick(state, ticks)
    actions = [...actions, ...result]

    result = Game.mine_tick(state, ticks)
    actions = [...actions, ...result]

    var result = this.colonies_tick(state, ticks)
    actions = [...actions, ...result]

    return actions
  }

  create_colony(target) {
    var colony = {
      name: 'Unnamed',
      coords: target,
      population: 1,
      funding: 0,
      id:    this.colony_id++,
      goods: [Utils.random(0, Common.GOODS_LIST.length-1),
              Utils.random(0, Common.GOODS_LIST.length-1)],
      lastTick: Date.now(),
    }
    return colony
  }
}

export function sector_id(i,j) {
  return 1 + i +  SECTOR_WIDTH * j
}

export function colony_id(sector,i,j) {
  return  (COLONY_WIDTH * COLONY_HEIGHT) * (sector-1) + (i + COLONY_HEIGHT * j) + 1
}

export function sector_x(i,j) {
  return STAGE_PADDING + i*(STAGE_WIDTH - 2*STAGE_PADDING)/SECTOR_WIDTH - SECTOR_SQUARE/2
}

export function sector_y(i,j) {
  return STAGE_PADDING + j*(STAGE_HEIGHT - 2*STAGE_PADDING)/SECTOR_HEIGHT - SECTOR_SQUARE/2
}

export function colony_x(i,j) {
  return STAGE_PADDING + i*(STAGE_WIDTH - 2*STAGE_PADDING)/COLONY_WIDTH
}

export function colony_y(i,j) {
  return STAGE_PADDING + j*(STAGE_HEIGHT - 2*STAGE_PADDING)/COLONY_HEIGHT
}

export function colony_subcoords(csid) {
  csid = csid - 1
  return [csid % COLONY_WIDTH, Math.floor(csid / COLONY_WIDTH)]
}

export function sector_coords(sid) {
  sid = sid - 1
  return [sid % SECTOR_WIDTH, Math.floor(sid / SECTOR_WIDTH)]
}

export function colony_coords(cid) {
  cid = cid- 1
  var rem = cid % (COLONY_WIDTH*COLONY_HEIGHT)
  var sector = Math.floor(cid / (COLONY_WIDTH*COLONY_HEIGHT))

  return [sector+1, rem+1]
}

export const Game = new Logic()
